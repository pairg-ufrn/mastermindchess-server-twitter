import java.io.IOException;
import java.net.*;


public class NetworkHandler {
	int port;
	int bufferSize;
	String receivedMessage;
	
	DatagramSocket serverSocket;
	
	String serverIP = "127.0.0.1";
	int serverPort = 20315;
	PropertiesDAO properties;
	
	public NetworkHandler() {
		properties = new PropertiesDAO();
		
		properties.loadProperties();
		
		serverPort = Integer.parseInt(properties.getProperties().getProperty("serverPort"));
		serverIP = properties.getProperties().getProperty("serverIP");
		
		

		try {
			serverSocket = new DatagramSocket(serverPort);
			System.out.println("Listening on port " + serverPort);
		} 
		catch (SocketException e) {
			
			e.printStackTrace();
			
		}
		
	}
	
	
	void receivePacket() throws IOException{
		
		byte[] receiveData = new byte[1024];
		
		DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
		
        serverSocket.receive(receivePacket);
        
        receivedMessage = new String( receivePacket.getData() );
			
	}
}
