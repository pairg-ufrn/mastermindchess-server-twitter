import twitter4j.*;
import twitter4j.auth.AccessToken;


public class TwitterModule {
	
	PropertiesDAO properties;
	
	String consumerKey;
	String consumerSecret;
	String accessToken;
	String accessTokenSecret;
	
	TwitterFactory twitterFactory;
	Twitter twitter;
	
	void initialize(){
		properties = new PropertiesDAO();
		
		properties.loadProperties();
		
		//Your Twitter App's Consumer Key
        consumerKey = properties.getProperties().getProperty("consumerKey");
 
        //Your Twitter App's Consumer Secret
        consumerSecret = properties.getProperties().getProperty("consumerSecret");
 
        //Your Twitter Access Token
        accessToken = properties.getProperties().getProperty("accessToken");
 
        //Your Twitter Access Token Secret
        accessTokenSecret = properties.getProperties().getProperty("accessTokenSecret");
        
        //Instantiate a re-usable and thread-safe factory
        twitterFactory = new TwitterFactory();
 
        //Instantiate a new Twitter instance
        twitter = twitterFactory.getInstance();
 
        //setup OAuth Consumer Credentials
        twitter.setOAuthConsumer(consumerKey, consumerSecret);
 
        //setup OAuth Access Token
        twitter.setOAuthAccessToken(new AccessToken(accessToken, accessTokenSecret));
        
	}
	
	
	Status tweet(String message){
		
		StatusUpdate statusUpdate = new StatusUpdate(
                //your tweet or status message
                message);
		
		Status response = null;
		
		try {
			response = twitter.updateStatus(statusUpdate);
		} 
		catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return response;
		
	}
	

	/**
	 * @param args
	 */
	

}
