import java.io.IOException;

import twitter4j.TwitterException;


public class Application {
	static TwitterModule twitterModule;
	
	
	public static void main(String[] args) throws IOException, TwitterException{
		twitterModule = new TwitterModule();
		twitterModule.initialize();
		NetworkHandler network = new NetworkHandler();
 
        //Instantiate and initialize a new twitter status update
        Boolean var = true;
		while(var){
			
			network.receivePacket();
			
			twitterModule.tweet(network.receivedMessage);
		}
	}
}
