import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class PropertiesDAO {
	
	static Properties _properties;
	InputStream input;
	String filename;
	
	public PropertiesDAO() {
		_properties = new Properties();
		input = null;
	}
	
	void loadProperties() {
		
		try{
			input = new FileInputStream("config.properties");
			 
			// load a properties file
			_properties.load(input);
	 
		}
		
		catch(IOException ex) {
			ex.printStackTrace();
		}
		
		finally {
			
			if (input != null) {
				
				try {
					input.close();
				} 
				
				catch (IOException e) {
					e.printStackTrace();
				}
				
			}
			
		}
		
	}
	
	
	public Properties getProperties() {
		return _properties;
	}

	public void setProperties(Properties _properties) {
		this._properties = _properties;
	}

	public InputStream getInput() {
		return input;
	}

	public void setInput(InputStream input) {
		this.input = input;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
}
